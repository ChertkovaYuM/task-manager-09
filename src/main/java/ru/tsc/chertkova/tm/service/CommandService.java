package ru.tsc.chertkova.tm.service;

import ru.tsc.chertkova.tm.api.ICommandRepository;
import ru.tsc.chertkova.tm.api.ICommandService;
import ru.tsc.chertkova.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getCommands() {
        return commandRepository.getCommands();
    }

}
